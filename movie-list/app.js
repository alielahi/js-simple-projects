class Movie {
    constructor(title, year, imdb, movieID) {
        this.title = title;
        this.year = year;
        this.imdb = imdb;
        this.movieID = movieID;
    }
}

class Store {
    static getMovies() {
        let movies;
        if (localStorage.getItem("movies") === null) {
            movies = [];
        } else {
            movies = JSON.parse(localStorage.getItem("movies"));
        }
        return movies;
    }

    static addMovie(movie) {
        const movies = Store.getMovies();
        movies.push(movie);

        localStorage.setItem("movies", JSON.stringify(movies));
    }

    static removeMovie(movieID) {
        const movies = Store.getMovies();

        movies.forEach((movie, index) => {
            if (movie.movieID === movieID) {
                movies.splice(index, 1);
            }
        });
        localStorage.setItem("movies", JSON.stringify(movies));
    }
}

class UI {
    static displaymovie() {

        const movies = Store.getMovies();
        
        movies.forEach((movie) => {
            UI.addMovieToList(movie);
        });
    }

    static addMovieToList(movie) {
        const list = document.querySelector("#movie-list");
        const row = document.createElement("tr");

        row.innerHTML = `
            <td>${movie.title}</td>
            <td>${movie.year}</td>
            <td>${movie.imdb}</td>
            <td>${movie.movieID}</td>
            <td><a href="#" class="btn btn-danger btn-sm delete">Delete</a></td>
        `;

        list.appendChild(row);
    }

    static removeMovieFromList(el) {
        if (el.classList.contains("delete")) {
            el.parentElement.parentElement.remove();
        }
    }

    static showAlert(message, className) {
        const container = document.querySelector(".container");
        const div = document.createElement("div");
        const form = document.querySelector("#movie-form");
        div.style.textAlign = "center";
        div.className = `alert alert-${className}`;
        div.appendChild(document.createTextNode(message));
        container.insertBefore(div, form);

        setTimeout(() => {
            document.querySelector(".alert").remove();
        }, 2000);
    }

    static clearFields() {
        document.querySelector("#title").value = "";
        document.querySelector("#year").value = "";
        document.querySelector("#imdb").value = "";
    }
}

document.addEventListener("DOMContentLoaded", UI.displaymovie());

// Event : Add a Book

let movieID = 0;
document.querySelector("#movie-form").addEventListener("submit", (e) => {
    // Get the values
    const title = document.querySelector("#title").value;
    const year = document.querySelector("#year").value;
    const imdb = document.querySelector("#imdb").value;
    movieID = movieID + 1;
    // Validation
    if (title == "" || year == "" || imdb == "") {
        UI.showAlert("Fill in bitch", "danger");
    } else {
        // Instantiate movie
        const movie = new Movie(title, year, imdb, movieID.toString(10));

        // Add movie to the UI
        UI.addMovieToList(movie);

        // Add movie to the localstorage
        Store.addMovie(movie);

        UI.showAlert("The Movie successfully added", "success");
        event.preventDefault();

        // Clear the fields
        UI.clearFields();
    }
});

// Remove a movie

document.querySelector("#movie-list").addEventListener("click", (e) => {
    UI.removeMovieFromList(e.target);
    console.log(e.target.parentElement.previousElementSibling.textContent)
    Store.removeMovie(e.target.parentElement.previousElementSibling.textContent)
    UI.showAlert("The Movie removed", "warning");
});
